﻿namespace formsoru5
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst1ders = new System.Windows.Forms.ListBox();
            this.lst2ders = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnsoladogru = new System.Windows.Forms.Button();
            this.btnsagadogru = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lst1ders
            // 
            this.lst1ders.FormattingEnabled = true;
            this.lst1ders.Items.AddRange(new object[] {
            "biyoloji",
            "matematik",
            "geometri",
            "nesne tabanlı programlama",
            "fizik ",
            "tarih"});
            this.lst1ders.Location = new System.Drawing.Point(30, 74);
            this.lst1ders.Name = "lst1ders";
            this.lst1ders.Size = new System.Drawing.Size(140, 95);
            this.lst1ders.TabIndex = 0;
            // 
            // lst2ders
            // 
            this.lst2ders.FormattingEnabled = true;
            this.lst2ders.Location = new System.Drawing.Point(284, 74);
            this.lst2ders.Name = "lst2ders";
            this.lst2ders.Size = new System.Drawing.Size(154, 95);
            this.lst2ders.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = " toplam seçilebilecek dersler:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "toplam seçilen ders :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(153, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(136, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(21, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 52);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(274, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(198, 51);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // btnsoladogru
            // 
            this.btnsoladogru.Location = new System.Drawing.Point(188, 127);
            this.btnsoladogru.Name = "btnsoladogru";
            this.btnsoladogru.Size = new System.Drawing.Size(75, 23);
            this.btnsoladogru.TabIndex = 8;
            this.btnsoladogru.Text = "<<<";
            this.btnsoladogru.UseVisualStyleBackColor = true;
            this.btnsoladogru.Click += new System.EventHandler(this.Btnsoladogru_Click);
            // 
            // btnsagadogru
            // 
            this.btnsagadogru.Location = new System.Drawing.Point(188, 166);
            this.btnsagadogru.Name = "btnsagadogru";
            this.btnsagadogru.Size = new System.Drawing.Size(75, 23);
            this.btnsagadogru.TabIndex = 9;
            this.btnsagadogru.Text = ">>>";
            this.btnsagadogru.UseVisualStyleBackColor = true;
            this.btnsagadogru.Click += new System.EventHandler(this.Btnsagadogru_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 223);
            this.Controls.Add(this.btnsagadogru);
            this.Controls.Add(this.btnsoladogru);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lst2ders);
            this.Controls.Add(this.lst1ders);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lst1ders;
        private System.Windows.Forms.ListBox lst2ders;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnsoladogru;
        private System.Windows.Forms.Button btnsagadogru;
    }
}

